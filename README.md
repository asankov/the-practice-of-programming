# The Practice of Programming

This repository contains the exercises from the book "The Practice of Programming" by Brian W. Kernighan and Rob Pike.

The book can be bought here: https://www.amazon.de/-/en/Brian-W-Kernighan-ebook/dp/B00HU50A12/

## Table of Contents

### [Chapter 1: Style](chapter-1)

#### [1.1 Names](chapter-1/1.1-names)
#### [1.2 Expressions and Statements](chapter-1/1.2-expressions-and-statements)
#### [1.3 Consistency and Idioms](chapter-1/1.3-consistency-and-idioms)
#### [1.4 Function Macros](chapter-1/1.4-function-macros)
#### [1.5 Magic Numbers](chapter-1/1.5-magic-numbers)
#### [1.6 Comments](chapter-1/1.6-comments)
#### [1.7 Why Bother?](chapter-1/1.7-why-bother)

### [Chapter 2: Algorithms and Data structures](chapter-2)
#### [2.1 Searching](chapter-2/2.1-searching)
#### [2.2 Sorting](chapter-2/2.2-sorting)
#### [2.3 Libraries](chapter-2/2.3-libraries)
#### [2.4 A Java Quicksort](chapter-2/2.4-java-quicksort)
#### [2.5 O-Notation](chapter-2/2.5-o-notation)
#### [2.6 Growing Arrays](chapter-2/2.6-growing-arrays)
#### [2.7 Lists](chapter-2/2.7-lists)
#### [2.8 Trees](chapter-2/2.8-trees)
#### [2.9 Hash Tables](chapter-2/2.9-hash-tables)

### [Chapter 3: Design and Implementation](chapter-3)
#### [3.1 The Markov Chain Algorithm](chapter-3/3.1-the-markov-chain-algorithm)
#### [3.2 Data Structure Alternatives](chapter-3/3.2-data-structure-alternatives)
#### [3.3 Building the Data Structure in C](chapter-3/3.3-building-the-data-structure-in-c)
#### [3.4 Generating Output](chapter-3/3.4-generating-output)
#### [3.5 Java](chapter-3/3.5-java)
#### [3.6 C++](chapter-3/3.6-c++)
#### [3.7 Awk and Perl](chapter-3/3.7-awk-and-perl)
#### [3.8 Performance](chapter-3/3.8-performance)
#### [3.9 Lessons](chapter-3/3.9-lessons)

### [Chapter 4: Interfaces](chapter-4)
#### [4.1 Comma-Separated Values](chapter-4/4.1-comma-separated-values)
#### [4.2 A Prototype Library](chapter-4/4.2-a-prototype-library)
#### [4.3 A Library For Others](chapter-4/4.3-a-library-for-others)
#### [4.4 A C++ Implementation](chapter-4/4.4-a-c++-implementation)
#### [4.5 Interface Principles](chapter-4/4.5-interface-principles)
#### [4.6 Resource Management](chapter-4/4.6-resource-management)
#### [4.7 Abort, Retry, Fail?](chapter-4/4.7-abort-retry-fail)
#### [4.8 User Interfaces](chapter-4/4.8-user-interfaces)

## License
This work is licensed under MIT license. For more info see [LICENSE.md](LICENSE.md)
